package com.instil.rebelcon.client.model

enum class CourseDifficulty {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED
}