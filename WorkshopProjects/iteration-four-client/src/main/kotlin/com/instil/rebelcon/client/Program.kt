package com.instil.rebelcon.client

import com.instil.rebelcon.client.gui.CourseBookingApp
import tornadofx.*

fun main(args: Array<String>) {
    launch<CourseBookingApp>(args)
}
