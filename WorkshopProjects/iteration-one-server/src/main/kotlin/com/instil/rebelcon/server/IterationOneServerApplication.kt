package com.instil.rebelcon.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class IterationOneServerApplication

fun main(args: Array<String>) {
	runApplication<IterationOneServerApplication>(*args)
}
