package demos.rest.services.calculator;

public enum CalcOperation {
	ADD,
	SUBTRACT,
	MULTIPLY,
	DIVIDE
}
