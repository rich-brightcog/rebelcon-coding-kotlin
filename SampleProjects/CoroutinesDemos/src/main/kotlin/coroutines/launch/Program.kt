package coroutines.launch

import coroutines.shared.pingSlowServer
import kotlinx.coroutines.*
import java.time.LocalTime
import java.time.temporal.ChronoUnit

fun main() = runBlocking {
    println("Program starts")
    runDemo().join()
    println("Program ends")
}

suspend fun runDemo() = GlobalScope.launch {
    println("Work starts")
    val start = LocalTime.now()

    val jobs = mutableListOf<Job>()
    jobs += launch { println(pingSlowServer(8)) }
    jobs += launch { println(pingSlowServer(6)) }
    jobs += launch { println(pingSlowServer(4)) }
    jobs += launch { println(pingSlowServer(2)) }

    jobs.forEach { it.join() }
    val stop = LocalTime.now()
    println("Work complete in ${start.until(stop, ChronoUnit.SECONDS)} seconds")
}




