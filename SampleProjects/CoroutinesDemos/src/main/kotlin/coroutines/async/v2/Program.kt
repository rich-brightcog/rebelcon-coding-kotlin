package coroutines.async.v2

import coroutines.shared.pingSlowServer
import kotlinx.coroutines.*
import java.time.LocalTime
import java.time.temporal.ChronoUnit.SECONDS

fun main() = runBlocking {
    println("Program starts")
    runDemo().join()
    println("Program ends")
}

fun runDemo() = GlobalScope.launch {
    fun pingSlowServerAsync(timeout: Int) = async {
        pingSlowServer(timeout)
    }
    println("Work starts")
    val start = LocalTime.now()

    val deferredJobs = mutableListOf<Deferred<String>>()
    deferredJobs += pingSlowServerAsync(8)
    deferredJobs += pingSlowServerAsync(6)
    deferredJobs += pingSlowServerAsync(4)
    deferredJobs += pingSlowServerAsync(2)

    val results = deferredJobs
                         .map { it.await() }
                         .joinToString(
                                 prefix = "\"",
                                 postfix = "\"",
                                 separator = ", " )
    println("Results are $results")
    val stop = LocalTime.now()
    println("Work complete in ${start.until(stop,SECONDS)} seconds")
}



