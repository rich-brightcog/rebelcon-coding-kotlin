package coroutines.async.v1

import coroutines.shared.pingSlowServer
import kotlinx.coroutines.*
import java.time.LocalTime
import java.time.temporal.ChronoUnit.SECONDS

fun main() = runBlocking {
    println("Program starts")
    runDemo().join()
    println("Program ends")
}

fun runDemo() = GlobalScope.launch {
    println("Work starts")
    val start = LocalTime.now()

    val deferredJobs = mutableListOf<Deferred<String>>()
    deferredJobs += async { pingSlowServer(8) }
    deferredJobs += async { pingSlowServer(6) }
    deferredJobs += async { pingSlowServer(4) }
    deferredJobs += async { pingSlowServer(2) }

    val results = deferredJobs
                         .map { it.await() }
                         .joinToString(
                                 prefix = "\"",
                                 postfix = "\"",
                                 separator = ", " )
    println("Results are $results")
    val stop = LocalTime.now()
    println("Work complete in ${start.until(stop,SECONDS)} seconds")
}



