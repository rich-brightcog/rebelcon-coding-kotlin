package coroutines.async.v3

import coroutines.shared.pingSlowServer
import kotlinx.coroutines.*
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.time.temporal.ChronoUnit.SECONDS

fun main() = runBlocking {
    println("Program starts")
    runDemo().join()
    println("Program ends")
}

suspend fun pingSlowServerAsync(timeout: Int) = withContext(Dispatchers.IO) {
    report("Pinging server with $timeout")
    pingSlowServer(timeout)
}

fun report(text: String) {
    val fmt = DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM)
    val id = Thread.currentThread().id
    println("$text on thread $id at time ${fmt.format(LocalTime.now())}")
}

fun runDemo() = GlobalScope.launch {
    println("Work starts")
    val start = LocalTime.now()

    val msg1 = pingSlowServerAsync(8)
    report("Received $msg1")
    val msg2 = pingSlowServerAsync(6)
    report("Received $msg2")
    val msg3 = pingSlowServerAsync(4)
    report("Received $msg3")
    val msg4 = pingSlowServerAsync(2)
    report("Received $msg4")

    val stop = LocalTime.now()
    println("Work complete in ${start.until(stop,SECONDS)} seconds")
}



