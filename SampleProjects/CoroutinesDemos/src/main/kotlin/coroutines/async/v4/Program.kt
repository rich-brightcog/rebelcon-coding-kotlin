package coroutines.async.v4

import coroutines.shared.pingSlowServerAsyncV3
import kotlinx.coroutines.*
import java.time.LocalTime
import java.time.temporal.ChronoUnit.SECONDS

fun main() = runBlocking {
    println("Program starts")
    runDemo().join()
    println("Program ends")
}

fun runDemo() = GlobalScope.launch {
    println("Work starts")
    val start = LocalTime.now()

    val deferredJobs = mutableListOf<Deferred<String>>()
    deferredJobs += async { pingSlowServerAsyncV3(8) }
    deferredJobs += async { pingSlowServerAsyncV3(6) }
    deferredJobs += async { pingSlowServerAsyncV3(4) }
    deferredJobs += async { pingSlowServerAsyncV3(2) }

    val results = deferredJobs
        .map { it.await() }
        .joinToString(
            prefix = "\"",
            postfix = "\"",
            separator = ", " )
    println("Results are $results")

    val stop = LocalTime.now()
    println("Work complete in ${start.until(stop,SECONDS)} seconds")
}



